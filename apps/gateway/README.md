# Gateway

This Gateway is responsible for receiving the description of a HTTP request, executing it and returning back the response. The actual execution of the HTTP request is done via the `HTTPoison` library. This Gateway assumes that a node named `public_interface@127.0.0.1` is running, and tries to connect to it during the application initialization.

# Running

Run each instance with `iex --name gateway-<number>@127.0.0.1 -S mix`

Examples:
* `iex --name gateway-1@127.0.0.1 -S mix`
* `iex --name gateway-2@127.0.0.1 -S mix`
* `iex --name gateway-3@127.0.0.1 -S mix`
