defmodule Gateway.API do
  use GenServer

  def start_link do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def call_api(request) do
    GenServer.call(__MODULE__, {:call_api, request})
  end

  def history(client) do
    GenServer.call(__MODULE__, {:history, client})
  end

  # GenServer callbacks

  def handle_call({:call_api, request}, _from, state) do
    res =
      Gateway.RequestResolverSup
      |> Task.Supervisor.async(Gateway.RequestResolver, :call_api, [request])
      |> Task.await()
    {:reply, res, state}
  end

  def handle_call({:history, client}, _from, state) do
    res =
      Gateway.RequestResolverSup
    |> Task.Supervisor.async(Gateway.RequestResolver, :history, [client])
    |> Task.await()
    {:reply, res, state}
  end
end
