defmodule Gateway.History do
  use GenServer

  def start_link(table_name \\ :gateway_history) do
    GenServer.start_link(__MODULE__, table_name, name: __MODULE__)
  end

  def save(client, entry) do
    {results, bad_nodes} =
      :rpc.multicall(__MODULE__, :save_local, [client, entry], :timer.seconds(5))
    Enum.each(bad_nodes, &IO.puts("Failed nodes: #{&1}"))
    :ok
  end

  def save_local(client, entry) do
    GenServer.cast __MODULE__, {:save, client, entry}
  end

  def clean do
    GenServer.cast __MODULE__, :clean
  end

  def retrieve(client) do
    GenServer.call __MODULE__, {:retrieve, client}
  end

  def init(table_name) do
    :ets.new(table_name, [:bag, :protected, :named_table])
    {:ok, table_name}
  end

  def handle_cast({:save, client, entry}, table_name) do
    key = client_hash(client)
    :ets.insert(table_name, {key, entry})
    {:noreply, table_name}
  end

  def handle_cast(:clean, table_name) do
    :ets.delete_all_objects(table_name)
    {:noreply, table_name}
  end

  def handle_call({:retrieve, client}, _from, table_name) do
    key = client_hash(client)
    entries = table_name |> :ets.lookup(key) |> Enum.map(fn({k, v}) -> v end)
    {:reply, entries, table_name}
  end

  def handle_info({:DOWN, _ref, :process, _pid, _reason}, table_name) do
    :ets.delete(table_name)
    {:noreply, table_name}
  end

  defp client_hash(%{ip_addr: ip}) do
    :erlang.phash2(ip)
  end
end
