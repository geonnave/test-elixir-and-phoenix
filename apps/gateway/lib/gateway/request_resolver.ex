defmodule Gateway.RequestResolver do

  def call_api(_req = %{client: c, req_input: req_input}) do
    add_to_history(c, req_input)
    do_call_api(req_input)
  end

  def add_to_history(client, req_input) do
    Gateway.History.save(client, req_input)
  end

  def do_call_api(%{url: url, verb: verb, content_type: ctype,
                   accepted_content_type: actype, body: body, query_string: qs}) do
    headers =
      []
      |> add_header("Content-Type", ctype)
      |> add_header("Accept", actype)

    url = cond do
        is_binary(qs) -> url <> "?" <> qs
        is_list(qs) -> url <> "?" <> URI.encode_query(qs)
        true -> url
    end

    %{body: resp_body, headers: resp_headers} = HTTPoison.request!(verb, url, body, headers)
    {resp_headers, resp_body}
  end

  defp add_header(headers, _, nil), do: headers
  defp add_header([], h, v), do: [{h, v}]
  defp add_header(headers, h, v), do: [{h, v} | headers]

  def history(client) do
    Gateway.History.retrieve(client)
  end

end
