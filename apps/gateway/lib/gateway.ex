defmodule Gateway do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    # Define workers and child supervisors to be supervised
    children = [
      supervisor(Task.Supervisor, [[name: Gateway.RequestResolverSup]]),
      worker(Gateway.History, []),
      worker(Gateway.API, []),
    ]

    HTTPoison.start

    Node.connect(:"public_interface@127.0.0.1")

    opts = [strategy: :one_for_one, name: Gateway.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
