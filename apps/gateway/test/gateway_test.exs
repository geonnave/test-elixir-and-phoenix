defmodule GatewayTest do
  use ExUnit.Case
  doctest Gateway

  @req_input %{url: "http://date.jsontest.com/",
               verb: :get,
               content_type: "application/json",
               accepted_content_type: "application/json",
               body: "", query_string: []
              }
  @client1 %{ip_addr: "1.2.3.4"}
  @client2 %{ip_addr: "2.2.2.2"}

  @req %{client: @client1, req_input: @req_input}

  setup do
    Gateway.History.start_link
    on_exit fn -> Gateway.History.clean end
    :ok
  end

  test "call the gateway" do
    Gateway.API.start_link

    assert {_headers, _body} = Gateway.API.call_api(@req)
    assert {_headers, <<"{\"ip\"", _rest::binary>>} =
      Gateway.API.call_api(put_in(@req[:req_input][:query_string], [service: "ip"]))

    Gateway.API.history(@client1) |> IO.inspect
  end
end
