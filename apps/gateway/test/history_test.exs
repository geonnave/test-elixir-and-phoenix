defmodule HistoryTest do
  use ExUnit.Case, async: true
  doctest Gateway

  alias Gateway.History

  @req_input %{url: "http://date.jsontest.com/",
               verb: :get,
               content_type: "application/json",
               accepted_content_type: "application/json",
               body: "", query_string: []
              }
  @client1 %{ip_addr: "1.2.3.4"}
  @client2 %{ip_addr: "2.2.2.2"}

  @req %{client: @client1, req_input: @req_input}

  setup do
    History.start_link
    on_exit fn -> History.clean end
    :ok
  end

  test "store and retrieve history" do
    History.save(@client1, "data")
    History.save(@client1, "data2")
    assert ["data", "data2"] == History.retrieve(@client1)
  end

  test "store and retrieve history from more than 1 client" do
    History.save(@client1, "data")
    History.save(@client1, "data2")
    History.save(@client2, "foo")
    History.save(@client2, "foo2")
    History.save(@client2, "foo3")
    assert ["data", "data2"] == History.retrieve(@client1)
    assert ["foo", "foo2", "foo3"] == History.retrieve(@client2)
  end
end
