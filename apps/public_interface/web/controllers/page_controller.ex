defmodule PublicInterface.PageController do
  use PublicInterface.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
