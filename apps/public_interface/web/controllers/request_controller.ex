defmodule PublicInterface.RequestController do
  use PublicInterface.Web, :controller

  alias PublicInterface.Request

  def index(conn, _params) do
    # requests = Repo.all(Request)
    changeset = Request.changeset(%Request{})
    render(conn, "index.html", changeset: changeset)
  end

  def create(conn, %{"request" => request_params}) do
    ip_addr = conn.remote_ip |> ip_from_tuple() |> IO.inspect

    request_params = put_in(request_params["verb"], request_params["verb"] |> String.to_atom)
    request_params = for {k, v} <- request_params, into: %{}, do: {String.to_atom(k), v}

    request = %{client: %{ip_addr: ip_addr}, req_input: request_params}

    response = request_to_gateway(request)

    render(conn, "show.html", response: response)
  end

  def request_to_gateway(request = %{client: %{ip_addr: ip_addr}}) do
    number_of_gateways = length(Node.list)
    gateway_number = :erlang.phash(ip_addr, number_of_gateways)

    node_gateway = :"gateway-#{gateway_number}@127.0.0.1"

    GenServer.call({Gateway.API, node_gateway}, {:call_api, request}) |> inspect
  end

  defp ip_from_tuple({a,b,c,d}), do: "#{a}.#{b}.#{c}.#{d}"

end
