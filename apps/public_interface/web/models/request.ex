defmodule PublicInterface.Request do
  use PublicInterface.Web, :model

  schema "requests" do
    field :verb, :string
    field :url, :string
    field :content_type, :string
    field :accepted_content_type, :string
    field :body, :string
    field :query_string, :string

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:verb, :url, :content_type, :accepted_content_type, :body, :query_string])
    |> validate_required([:verb, :url, :content_type, :accepted_content_type])
  end
end
