# PublicInterface

This project consists in a Phoenix-based web application that allows a user to input parameters for making a HTTP request. After submiting the parameters in a form, the user is presented with a page that exhibits the result of the specified requests.

The actual request is processed by a Gateway, which is an Elixir application which acts like a Proxy. Such Gateway runs in many nodes (i.e distributed). The logic for choosing which node to pass the request is in the module `RequestController`.

# Running

Run with `iex --name public_interface@127.0.0.1 -S mix phoenix.server`

**Note**: with the current setup, if this phoenix app dies, the gateways must be manually restarted/reconnected.

# TODO

* Add tests
* Impelemt the API path `/history`
