defmodule PublicInterface.Repo.Migrations.CreateRequest do
  use Ecto.Migration

  def change do
    create table(:requests) do
      add :verb, :string
      add :url, :string
      add :content_type, :string
      add :accepted_content_type, :string
      add :body, :string
      add :query_string, :string

      timestamps()
    end

  end
end
