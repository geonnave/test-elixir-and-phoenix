defmodule PublicInterface.RequestTest do
  use PublicInterface.ModelCase

  alias PublicInterface.Request

  @valid_attrs %{accepted_content_type: "some content", body: "some content", content_type: "some content", query_string: "some content", url: "some content", verb: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Request.changeset(%Request{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Request.changeset(%Request{}, @invalid_attrs)
    refute changeset.valid?
  end
end
